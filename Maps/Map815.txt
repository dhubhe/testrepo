﻿


EVENT   1
 PAGE   1
  // condition: variable 126 >= 0
  ShowMessageFace("granberia_fc4",0,0,2,1)
  ShowMessage("【グランベリア】")
  ShowMessage("ほう、次の相手はお前か……")
  ShowMessage("我が剣、失望させるなよ！")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("【審判】")
  ShowMessage("それでは……試合開始ー！")
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x11,0x62,0x6f,0x73,0x73,0x5f,0x73,0x69,0x74,0x65,0x6e,0x6e,0x6f,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ChangeSwitch(2333,2333,0)
  Battle(0,1265,false,false)
  ChangeSwitch(2333,2333,1)
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x62,0x61,0x74,0x74,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("granberia_fc4",2,0,2,3)
  ShowMessage("【グランベリア】")
  ShowMessage("なんという腕……")
  ShowMessage("我が剣、及ばなかったか。")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("【審判】")
  ShowMessage("ルカ選手がグランベリア選手を破り、大会を制したー！")
  ShowMessage("なお優勝者には、豪華な賞品が贈られます！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventoryWeapon(1304,0,0,1,false)
  RunCommonEvent(889)
  Wait(60)
  ChangeVariable(121,121,2,0,1)
  RunCommonEvent(901)
  EndEventProcessing()
  0()
 PAGE   2
  // condition: variable 126 >= 1
  ShowMessageFace("arumaeruma_fc2",0,0,2,1)
  ShowMessage("【アルマエルマ】")
  ShowMessage("あら……次の相手はあなたなの？")
  ShowMessage("さあ、たっぷりと楽しみましょう♪")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("【審判】")
  ShowMessage("それでは……試合開始ー！")
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x11,0x62,0x6f,0x73,0x73,0x5f,0x73,0x69,0x74,0x65,0x6e,0x6e,0x6f,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ChangeSwitch(2333,2333,0)
  Battle(0,1264,false,false)
  ChangeSwitch(2333,2333,1)
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x62,0x61,0x74,0x74,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("arumaeruma_fc2",6,0,2,3)
  ShowMessage("【アルマエルマ】")
  ShowMessage("うふふっ、やるじゃない。")
  ShowMessage("もう足腰が立たなくなったわ……")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("【審判】")
  ShowMessage("ルカ選手がアルマエルマ選手を破り、大会を制したー！")
  ShowMessage("なお優勝者には、豪華な賞品が贈られます！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventoryWeapon(1860,0,0,1,false)
  RunCommonEvent(889)
  Wait(60)
  ChangeVariable(121,121,2,0,1)
  RunCommonEvent(901)
  EndEventProcessing()
  0()
 PAGE   3
  // condition: variable 126 >= 2
  ShowMessageFace("seitentaisei_fc1",0,0,2,1)
  ShowMessage("【孫悟空】")
  ShowMessage("この孫悟空様に挑むのは、お前達か！")
  ShowMessage("さあ、全力で行くぜ！")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("【審判】")
  ShowMessage("それでは……試合開始ー！")
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x62,0x6f,0x73,0x73,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ChangeSwitch(2333,2333,0)
  Battle(0,1266,false,false)
  ChangeSwitch(2333,2333,1)
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x62,0x61,0x74,0x74,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("seitentaisei_fc1",2,0,2,3)
  ShowMessage("【孫悟空】")
  ShowMessage("なんだよ、結構やるじゃないか……")
  ShowMessage("今日のところは勝ちを譲ってやるぜ。")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("【審判】")
  ShowMessage("ルカ選手が孫悟空選手を破り、大会を制したー！")
  ShowMessage("なお優勝者には、豪華な賞品が贈られます！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventoryWeapon(2070,0,0,1,false)
  RunCommonEvent(889)
  Wait(60)
  ChangeVariable(121,121,2,0,1)
  RunCommonEvent(901)
  EndEventProcessing()
  0()
 PAGE   4
  // condition: variable 126 >= 3
  ShowMessageFace("morrigan_fc1",0,0,2,1)
  ShowMessage("【モリガン】")
  ShowMessage("お前が、決勝戦の相手か……")
  ShowMessage("さあ、干物にしてやるぜ！")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("【審判】")
  ShowMessage("それでは……試合開始ー！")
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x10,0x62,0x6f,0x73,0x73,0x5f,0x6c,0x69,0x6c,0x69,0x74,0x68,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ChangeSwitch(2333,2333,0)
  Battle(0,1253,false,false)
  ChangeSwitch(2333,2333,1)
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x62,0x61,0x74,0x74,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("morrigan_fc1",2,0,2,3)
  ShowMessage("【モリガン】")
  ShowMessage("くっ、無様に負けちまうとはな……")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("【審判】")
  ShowMessage("ルカ選手がモリガン選手を破り、大会を制したー！")
  ShowMessage("なお優勝者には、豪華な賞品が贈られます！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventoryWeapon(1120,0,0,1,false)
  RunCommonEvent(889)
  Wait(60)
  ChangeVariable(121,121,2,0,1)
  RunCommonEvent(901)
  EndEventProcessing()
  0()
 PAGE   5
  // condition: variable 126 >= 4
  ShowMessageFace("astaroth_fc1",0,0,2,1)
  ShowMessage("【アスタロト】")
  ShowMessage("あなた達が相手になってくれるの？")
  ShowMessage("悪いけれど、容赦はしないわよ……")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("【審判】")
  ShowMessage("それでは……試合開始ー！")
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x10,0x62,0x6f,0x73,0x73,0x5f,0x6c,0x69,0x6c,0x69,0x74,0x68,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ChangeSwitch(2333,2333,0)
  Battle(0,1255,false,false)
  ChangeSwitch(2333,2333,1)
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x62,0x61,0x74,0x74,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("astaroth_fc1",2,0,2,3)
  ShowMessage("【アスタロト】")
  ShowMessage("私が負けた……？")
  ShowMessage("意外と悔しいわね……")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("【審判】")
  ShowMessage("ルカ選手がアスタロト選手を破り、大会を制したー！")
  ShowMessage("なお優勝者には、豪華な賞品が贈られます！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventoryWeapon(2269,0,0,1,false)
  RunCommonEvent(889)
  Wait(60)
  ChangeVariable(121,121,2,0,1)
  RunCommonEvent(901)
  EndEventProcessing()
  0()
 PAGE   6
  // condition: variable 126 >= 5
  ShowMessageFace("lilith2_fc1",0,0,2,1)
  ShowMessage("【リリス】")
  ShowMessage("さあ、勝負を始めましょうか。")
  ShowMessage("観客もあなたも、たっぷり楽しませてあげましょう……")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("【審判】")
  ShowMessage("それでは……試合開始ー！")
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x10,0x62,0x6f,0x73,0x73,0x5f,0x6c,0x69,0x6c,0x69,0x74,0x68,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ChangeSwitch(2333,2333,0)
  Battle(0,1273,false,false)
  ChangeSwitch(2333,2333,1)
  SetBattleBGM(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x62,0x61,0x74,0x74,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("lilith2_fc1",2,0,2,3)
  ShowMessage("【リリス】")
  ShowMessage("後れを取ってしまいましたか……")
  ShowMessage("今は、あなたの力を称えましょう。")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("【審判】")
  ShowMessage("ルカ選手がリリス選手を破り、大会を制したー！")
  ShowMessage("なお優勝者には、豪華な賞品が贈られます！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventoryWeapon(3569,0,0,1,false)
  RunCommonEvent(889)
  Wait(60)
  ChangeVariable(121,121,2,0,1)
  RunCommonEvent(901)
  EndEventProcessing()
  0()



EVENT   2
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("次のフロアに進みますか？")
  ShowChoices(strings("まだ行かない","次のフロアに進む"),1)
  IfPlayerPicksChoice(0,null)
   205(-1,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x07,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x09,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
   EndEventProcessing()
   0()
  IfPlayerPicksChoice(1,null)
   ChangeVariable(121,121,2,0,1)
   RunCommonEvent(901)
   EndEventProcessing()
   0()
  404()
  0()



EVENT   3
 PAGE   1
  If(1,126,0,0,0)
   ShowMessageFace("",0,0,2,1)
   ShowMessage("【審判】")
   ShowMessage("圧倒的な力で、優勝に王手を掛けたグランベリア選手！")
   ShowMessage("それに対するは、前大会優勝者のルカ選手だー！")
   0()
  EndIf()
  If(1,126,0,1,0)
   ShowMessageFace("",0,0,2,2)
   ShowMessage("【審判】")
   ShowMessage("圧倒的な力で、優勝に王手を掛けたアルマエルマ選手！")
   ShowMessage("それに対するは、前大会優勝者のルカ選手だー！")
   0()
  EndIf()
  If(1,126,0,2,0)
   ShowMessageFace("",0,0,2,3)
   ShowMessage("【審判】")
   ShowMessage("圧倒的な力で、優勝に王手を掛けた斉天大聖選手！")
   ShowMessage("それに対するは、前大会優勝者のルカ選手だー！")
   0()
  EndIf()
  If(1,126,0,3,0)
   ShowMessageFace("",0,0,2,4)
   ShowMessage("【審判】")
   ShowMessage("圧倒的な力で、優勝に王手を掛けたモリガン選手！")
   ShowMessage("それに対するは、前大会優勝者のルカ選手だー！")
   0()
  EndIf()
  If(1,126,0,4,0)
   ShowMessageFace("",0,0,2,5)
   ShowMessage("【審判】")
   ShowMessage("圧倒的な力で、優勝に王手を掛けたアスタロト選手！")
   ShowMessage("それに対するは、前大会優勝者のルカ選手だー！")
   0()
  EndIf()
  If(1,126,0,5,0)
   ShowMessageFace("",0,0,2,6)
   ShowMessage("【審判】")
   ShowMessage("圧倒的な力で、優勝に王手を掛けたリリス選手！")
   ShowMessage("それに対するは、前大会優勝者のルカ選手だー！")
   0()
  EndIf()
  If(0,4,0)
   ShowMessageFace("alice_fc5",2,0,2,7)
   ShowMessage("【アリス】")
   ShowMessage("ここは、コロシアムか……")
   ShowMessage("腕に自信がなければ、退いた方が良いかもしれんな。")
   0()
  EndIf()
  If(0,5,0)
   ShowMessageFace("iriasu_fc4",2,0,2,8)
   ShowMessage("【イリアス】")
   ShowMessage("ここはコロシアムですか……")
   ShowMessage("腕に自信がないなら、退くのも手ですよ。")
   0()
  EndIf()
  TemporaryRemoveEvent()
  0()
